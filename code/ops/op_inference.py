import logging
import os
import tempfile
import SimpleITK as sitk

import monai.deploy.core as md
from monai.deploy.core import ExecutionContext, InputContext, IOType, Operator, OutputContext, DataPath


@md.input("", sitk.Image, IOType.IN_MEMORY)
@md.output("", DataPath, IOType.DISK)
class Predict(Operator):
    def __init__(self, task_id: str):
        self.logger = logging.getLogger(f"{__name__}.{type(self).__name__}")
        self.task_id = task_id
        super().__init__()

    def compute(self, op_input: InputContext, op_output: OutputContext, context: ExecutionContext):

        img = op_input.get()

        output_folder = str(op_output.get().path)

        self.predict(img, self.task_id, output_folder)

    def predict(self, img, task_id, output_folder):
        with tempfile.TemporaryDirectory() as tmp_in:
            sitk.WriteImage(img, os.path.join(tmp_in, "tmp_0000.nii.gz"))

            self.logger.info(f"Predicting for task {task_id}")

            os.system(f"nnUNet_predict -t {task_id} -tr nnUNetTrainerV2 -i {tmp_in} -o {output_folder}")