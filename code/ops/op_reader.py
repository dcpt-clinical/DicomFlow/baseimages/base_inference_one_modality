import logging
import SimpleITK as sitk
import monai.deploy.core as md
from monai.deploy.core import ExecutionContext, DataPath, InputContext, IOType, Operator, OutputContext


@md.input("", DataPath, IOType.DISK)
@md.output("", sitk.Image, IOType.IN_MEMORY)
class DataLoader(Operator):
    '''
    Reads the dicom series from the input directory and returns the image and the input directory.
    '''
    def __init__(self):
        self.logger = logging.getLogger(f"{__name__}.{type(self).__name__}")
        super().__init__()

    def compute(self, op_input: InputContext, op_output: OutputContext, context: ExecutionContext):
        in_path = str(op_input.get().path)

        reader = sitk.ImageSeriesReader()
        reader.LoadPrivateTagsOn()
        dicom_names = reader.GetGDCMSeriesFileNames(in_path)
        reader.SetFileNames(dicom_names)
        img = reader.Execute()

        op_output.set(value=img)
