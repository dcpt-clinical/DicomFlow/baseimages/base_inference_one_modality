import os
from monai.deploy.core import Application

from ops.op_inference import Predict
from ops.op_reader import DataLoader


class InferenceApplication(Application):
    '''
    This is the main application class for the inference pipeline.
    It differs from the other applications in that all data is passed as dicts of arrays, with the keys being the task id.
    This makes adding new tasks easier, as the application does not need to be modified,
    all that is needed is a new meta_info file as well as a new model.
    However, this also means that the application is not as efficient as it could be about memory usage,
    and this way of structuring the application sort of goes against the monai.deploy philosophy.
    '''
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def compose(self):
        task_id = os.environ.get("TASK_ID")

        dataloader_op = DataLoader()
        predict_op = Predict(task_id=task_id)

        # Flows
        self.add_flow(dataloader_op, predict_op, {"": ""})


if __name__ == "__main__":
    InferenceApplication(do_run=True)
