FROM registry.gitlab.com/dcpt-clinical/dicomflow/baseimages/base_nnunet_v1:v2.0_9cebd2ee

COPY . ./
# We are in /opt/app/

ENV RESULTS_FOLDER=/opt/app/model

ENV INPUT=/input
ENV OUTPUT=/output

CMD /opt/conda/bin/python3 /opt/app/code/app.py --input $INPUT --output $OUTPUT